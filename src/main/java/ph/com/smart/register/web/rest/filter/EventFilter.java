package ph.com.smart.register.web.rest.filter;

import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import ph.com.smart.register.model.Event;
import ph.com.smart.register.model.Event.Status;
import ph.com.smart.register.model.Registrant;
import ph.com.smart.register.web.rest.util.SpecificationFilter;

public class EventFilter extends SpecificationFilter<Event>{

	public EventFilter(HttpServletRequest request){
		super(request);
	}
	
	@Override
	public void createPredicates(Root<Event> root, CriteriaQuery<?> criteria, CriteriaBuilder criteriaBuilder, List<Predicate> predicates) {
		Boolean active = getBooleanParam("active");
		if(active!=null){
			Predicate activePredicate = criteriaBuilder.equal(root.<Boolean>get("active"), active);
			predicates.add(activePredicate); 
		}
		
		String status = getParam("status");
		if(status!=null){
			Date today = new Date();
			Status eventStatus = Status.valueOf(status);
			if(eventStatus.equals(Status.ENDED)){

				Predicate endedPredicate = criteriaBuilder.lessThan(root.<Date>get("dateEnd"), today);
				predicates.add(endedPredicate); 
			}else if(eventStatus.equals(Status.ONGOING)){

				Predicate ongoing1Predicate = criteriaBuilder.lessThanOrEqualTo(root.<Date>get("dateStart"), today);
				predicates.add(ongoing1Predicate); 

				Predicate ongoing2Predicate = criteriaBuilder.greaterThanOrEqualTo(root.<Date>get("dateEnd"), today);
				predicates.add(ongoing2Predicate); 
			}else if(eventStatus.equals(Status.UPCOMING)){
				Predicate upcomingPredicate = criteriaBuilder.greaterThan(root.<Date>get("dateStart"), today);
				predicates.add(upcomingPredicate); 
			}
		}
		
	}

}
