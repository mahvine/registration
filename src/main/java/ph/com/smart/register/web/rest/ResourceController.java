package ph.com.smart.register.web.rest;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import net.coobird.thumbnailator.Thumbnails;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.common.io.Files;

@RestController
@ConfigurationProperties(prefix = "resources")
public class ResourceController {

	private Logger logger = (Logger) LoggerFactory.getLogger(ResourceController.class);

	private String dataDirectory;

	@RequestMapping(value = "/resources", method = RequestMethod.POST)
	public GenericResponse handleFileUpload(
			@RequestParam("file") MultipartFile file) {
		logger.debug("file:{}",file);
		String extension = Files.getFileExtension(file.getOriginalFilename());
		logger.info("File upload receive {} |file extension = {}" ,file.getOriginalFilename(), extension);
		Date date = new Date();
		@SuppressWarnings("deprecation")
		String subDirectory = date.getYear() + "_" + date.getMonth();
		String resourcePath = null;
		resourcePath = subDirectory + "/"+ RandomStringUtils.randomAlphanumeric(11);
		if(extension.isEmpty()){
			resourcePath += ".png";
		}else{
			resourcePath += "." + extension;
		}
		logger.debug("resource Path:{}",resourcePath);
		if (!file.isEmpty()) {
			try {

				File resourceFile = new File(dataDirectory + "/" + resourcePath);
				String absolutePath = resourceFile.getAbsolutePath();
				logger.debug("Attempting to write file:" + absolutePath);
				Files.createParentDirs(resourceFile);

//				Files.write(bytes, resourceFile);
				
				Thumbnails.of(file.getInputStream()).size(1920 , 1080).toFile(dataDirectory + "/" + resourcePath);
				logger.debug("write to file complete:" + resourceFile.getAbsolutePath());
				Map<String, Object> data = new HashMap<String, Object>();
				data.put("resourcePath",resourcePath);
				logger.debug("response data:{}",data);
				return new GenericResponse(data);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} else {
			throw new RuntimeException("Invalid file upload");
		}
	}


	@RequestMapping(value = "/resources/{subdirectory}/{file_name:.+}", method = RequestMethod.GET)
	@ResponseBody
	public FileSystemResource getFile(
			@PathVariable("subdirectory") String subdirectory,
			@PathVariable("file_name") String filename) {
		logger.debug("retrieving :" + subdirectory + "/" + filename);
		return new FileSystemResource(new File(dataDirectory + "/"
				+ subdirectory + "/" + filename));
	}

	public String getDataDirectory() {
		return dataDirectory;
	}

	public void setDataDirectory(String dataDirectory) {
		this.dataDirectory = dataDirectory;
	}

	@JsonInclude(Include.NON_NULL)
	public static class GenericResponse {
		
		public Object data;
		
		public GenericResponse(String key, Object value){
			Map<String,Object> dataMap =  new HashMap<String,Object>();
			dataMap.put(key,value);
			data = dataMap;
		}
		
		public GenericResponse(Object data){
			this.data = data;
		}
	
	}
}
