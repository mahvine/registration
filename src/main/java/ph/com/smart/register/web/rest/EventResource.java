package ph.com.smart.register.web.rest;

import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import ph.com.smart.register.model.Event;
import ph.com.smart.register.model.Event.Status;
import ph.com.smart.register.model.Registrant;
import ph.com.smart.register.reports.RegistrantExcelBuilder;
import ph.com.smart.register.repository.EventRepository;
import ph.com.smart.register.repository.RegistrantRepository;
import ph.com.smart.register.repository.RequirementRepository;
import ph.com.smart.register.web.rest.filter.EventFilter;
import ph.com.smart.register.web.rest.filter.RegistrantFilter;
import ph.com.smart.register.web.rest.util.PaginationUtil;

@RestController
@RequestMapping("/register/api")
public class EventResource {
	
	private static final Logger logger = LoggerFactory.getLogger(EventResource.class);
	
	@Autowired
	EventRepository eventRepository;
	
	@Autowired
	RegistrantRepository registrantRepository;
	
	@Autowired
	RequirementRepository requirementRepository;
	
	@RequestMapping(value="/events",method=RequestMethod.POST)
	public void save(@RequestBody Event event){
		eventRepository.setAllToInActive();
		requirementRepository.save(event.requirements);
		if(event.id==null){
			event.dateCreated = new Date();
		}
		eventRepository.save(event);
	}
	
	
	@RequestMapping(value="/events", method=RequestMethod.GET)
	public ResponseEntity<List<Event>> listEvents(@RequestParam(value="page",defaultValue="1",required=false) int page,
			@RequestParam(value="per_page",defaultValue="10",required=false) int size,
			@RequestParam(value="direction",required=false,defaultValue="DESC") Direction direction,
			@RequestParam(value="sort",required=false,defaultValue="id") String sort,
			@RequestParam(value="status",required=false) Status status,
			HttpServletRequest request) throws URISyntaxException{
		PageRequest pageRequest=  PaginationUtil.generatePageRequest(page, size, new Sort(direction,sort));
		Page<Event> pageEvent = eventRepository.findAll(new EventFilter(request),pageRequest);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageEvent, "/api/events", page, size); 
		ResponseEntity<List<Event>> response = new ResponseEntity<List<Event>>(pageEvent.getContent(),headers,HttpStatus.OK);
		return response;
	}

	
	@RequestMapping(value="/events/{id}", method=RequestMethod.GET)
	public Event getEvent(@PathVariable("id") Long id){
		return eventRepository.findOne(id);
	}
	
	@RequestMapping(value="/events/{id}/registrants",method=RequestMethod.POST)
	public void saveRegistrant(@PathVariable("id") Long id, @RequestBody Registrant registrant){
		
		registrant.constructFullname();
		Event event = eventRepository.findOne(id);
		if(event==null){
			throw new RuntimeException("Invalid event");
		}
		registrant.event = event;
		if(registrant.id == null){
			registrant.created = new Date();
		}
		
		if(registrant.register){
			registrant.registered = new Date();
		}
		
		registrantRepository.save(registrant);
	}
	

	@RequestMapping(value="/events/{id}/registrants",method=RequestMethod.GET)
	public ResponseEntity<List<Registrant>> listEvents(@PathVariable("id") Long id, 
			@RequestParam(value="page",defaultValue="1",required=false) int page,
			@RequestParam(value="per_page",defaultValue="10",required=false) int size,
			@RequestParam(value="direction",required=false,defaultValue="DESC") Direction direction,
			@RequestParam(value="sort",required=false,defaultValue="id") String sort,
			@RequestParam(value="mobile",required=false) String mobile,
			HttpServletRequest request) throws URISyntaxException{
		PageRequest pageRequest=  PaginationUtil.generatePageRequest(page, size, new Sort(direction, sort));
		Event event = eventRepository.findOne(id);
		if(event==null){
			throw new RuntimeException("Invalid event");
		}
		
		Page<Registrant> pageRegistrant = registrantRepository.findAll(new RegistrantFilter(request, event),pageRequest);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageRegistrant, "/api/events/"+id+"/registrants", page, size); 
		ResponseEntity<List<Registrant>> response = new ResponseEntity<List<Registrant>>(pageRegistrant.getContent(),headers,HttpStatus.OK);
		return response;
	}
	

	@RequestMapping(value="/events/{id}/registrants/downloads",method=RequestMethod.GET)
	public ModelAndView downloadVehicles( ModelAndView mav,
	        HttpServletRequest request, 
	        HttpServletResponse response,
	        @PathVariable("id") Long eventId) {
		Event event = eventRepository.findOne(eventId);
		List<Registrant> registrants = registrantRepository.findAll(new RegistrantFilter(request, event));
	    mav.setView(new RegistrantExcelBuilder(registrants, event));
	    return mav;
	}


}
