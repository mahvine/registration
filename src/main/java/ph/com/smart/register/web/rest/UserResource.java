/**
 *
 */
package ph.com.smart.register.web.rest;

import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.context.support.SecurityWebApplicationContextUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.com.smart.register.model.User;
import ph.com.smart.register.model.User.Type;
import ph.com.smart.register.model.UserRole;
import ph.com.smart.register.repository.UserRepository;
import ph.com.smart.register.repository.UserRoleRepository;

/**
 * @author Jrdomingo
 * Jan 25, 2016 3:50:53 PM
 * TODO
 */
@RestController
@RequestMapping("/registration")
public class UserResource {
	
	private static final Logger logger = LoggerFactory.getLogger(UserResource.class);


	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	UserRoleRepository userRoleRepository;
	
	@RequestMapping(value="/users", method=RequestMethod.GET)
	public void changePassword(@RequestParam(value="username",required=false) String username, 
			@RequestParam(value="password", required=false) String password) throws Exception{
		if(username==null){
			org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			username = user.getUsername();
		}
		User user = userRepository.findByUsername(username);
		user.password = passwordEncoder.encode(password);
		userRepository.save(user);
		
		
		logger.info(username+" "+password);
	}

	@RequestMapping(value="/createUsers", method=RequestMethod.POST)
	public void createUser(@RequestParam("username") String username, 
			@RequestParam("password") String password, @RequestParam("type") User.Type userType){
		User user = new User();
		user.username = username;
		user.password = passwordEncoder.encode(password);
		user.enabled = true;
		userRepository.save(user);
		logger.info("created user:{}",user);
		if(userType.equals(User.Type.ADMIN)){
			UserRole userRole = new UserRole();
			userRole.user = user;
			userRole.role = User.Type.ADMIN.toString();
			userRoleRepository.save(userRole);
			logger.info("created role:{}",userRole);
		}

		UserRole userRole = new UserRole();
		userRole.user = user;
		userRole.role = User.Type.USER.toString();
		userRoleRepository.save(userRole);
		logger.info("created role:{}",userRole);
	}
	
	@PostConstruct
	public void createUsers(){
		List<User> users = userRepository.findAll();
        if(users.size()==0){
        	createUser("admin", "1234",Type.ADMIN);
        	createUser("smartadmin", "smart4321",Type.ADMIN);
        	createUser("user", "1234",Type.USER);
        	createUser("smartuser", "smart4321",Type.USER);

        	logger.info("created initial users");
        }else{
        	logger.info("has existing users");
        }
	}
	
}
