package ph.com.smart.register.web.rest.filter;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import ph.com.smart.register.model.Event;
import ph.com.smart.register.model.Registrant;
import ph.com.smart.register.web.rest.util.SpecificationFilter;

public class RegistrantFilter extends SpecificationFilter<Registrant>{

	private Event event;
	
	public RegistrantFilter(HttpServletRequest request, Event event){
		super(request);
		this.event = event;
	}
	
	@Override
	public void createPredicates(Root<Registrant> root, CriteriaQuery<?> criteria, CriteriaBuilder criteriaBuilder, List<Predicate> predicates) {
		predicates.add(criteriaBuilder.equal(root.<Event>get("event"), event));
		
		String firstname = getParam("firstname");
		if(firstname!=null){
			Predicate firstnamePredicate = criteriaBuilder.like(root.<String>get("firstname"), firstname);
			predicates.add(firstnamePredicate); 
		}
		
		String middlename = getParam("middlename");
		if(middlename!=null){
			Predicate middlenamePredicate = criteriaBuilder.like(root.<String>get("middlename"), middlename);
			predicates.add(middlenamePredicate); 
		}

		String lastname = getParam("lastname");
		if(lastname!=null){
			Predicate lastnamePredicate = criteriaBuilder.like(root.<String>get("lastname"), lastname);
			predicates.add(lastnamePredicate); 
		}

		String email = getParam("email");
		if(email!=null){
			Predicate emailPredicate = criteriaBuilder.like(root.<String>get("email"), email);
			predicates.add(emailPredicate); 
		}
		
		String mobile = getParam("mobile");
		if(mobile!=null){
			Predicate mobilePredicate = criteriaBuilder.like(root.<String>get("mobile"), mobile);
			predicates.add(mobilePredicate); 
		}
		
	}

}
