package ph.com.smart.register.web.rest.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

public abstract class SpecificationFilter<T> implements Specification<T>{
	
	private static final Logger logger = LoggerFactory.getLogger(SpecificationFilter.class);
	
	protected Map<String, String[]> parameters;

	public SpecificationFilter(final HttpServletRequest request){
		parameters = request.getParameterMap();
	}
	
	public Map<String,String[]> getParameters(){
		return parameters;
	}

	public String getParam(String key){
		String[] value = parameters.get(key);
		if(value!=null){
			if(value.length>0){
				return value[0];
			}
		}
		return null;
	}

	public Integer getIntParam(String key){
		String[] value = parameters.get(key);
		if(value!=null){
			if(value.length>0){
				return Integer.parseInt(value[0]);
			}
		}
		return null;
	}

	public Long getLongParam(String key){
		String[] value = parameters.get(key);
		if(value!=null){
			if(value.length>0){
				return Long.parseLong(value[0]);
			}
		}
		return null;
	}

	public Double getDoubleParam(String key){
		String[] value = parameters.get(key);
		if(value!=null){
			if(value.length>0){
				return Double.parseDouble(value[0]);
			}
		}
		return null;
	}

	public Boolean getBooleanParam(String key){
		String[] value = parameters.get(key);
		if(value!=null){
			if(value.length>0){
				return Boolean.valueOf(value[0]);
			}
		}
		return null;
	}

	public Date getDateParam(String key, String format){
		String[] value = parameters.get(key);
		if(value!=null){
			if(value.length>0){
				DateFormat df = new SimpleDateFormat(format);
				try{
					return df.parse(value[0]);
				}catch(RuntimeException e){
				} catch (ParseException e) {
				}
			}
		}
		return null;
	}
	
	public Date getDateParam(String key){
		return getDateParam(key,"MM/dd/yyyy");
	}
	
	@Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteria,CriteriaBuilder criteriaBuilder){
		final List<Predicate> predicates = new ArrayList<Predicate>();
		createPredicates(root, criteria, criteriaBuilder, predicates);
		logger.debug("predicates list size:{}",predicates.size());
		if(predicates.isEmpty()){
			return null;
		}
		return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
	}

	/**
	 * 	<b>Sample Code:</b><br>
	 * <code>
 	 *	Predicate greaterThanEqualDate = criteriaBuilder.greaterThanOrEqualTo(root.<Date>get("endDate"), date);<br>
 	 *	predicates.add(greaterThanEqualDate);
	 * </code>
	 */
	public abstract void createPredicates(Root<T> root, CriteriaQuery<?> criteria,CriteriaBuilder criteriaBuilder, List<Predicate> predicates);
	
		
}
