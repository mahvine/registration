package ph.com.smart.register.config;

import org.apache.catalina.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration.WebMvcAutoConfigurationAdapter;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;

@Configuration
public class WebMvcConfig  extends WebMvcAutoConfigurationAdapter  implements EmbeddedServletContainerCustomizer{
	
	private static final Logger log = LoggerFactory.getLogger(WebMvcConfig.class);

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        // forward requests to /admin and /user to their index.html
        registry.addViewController("/api-docs").setViewName("forward:/api-docs/index.html");
//        registry.addViewController("/").setViewName("forward:/app/index.html");
        registry.addViewController("/admin").setViewName("forward:/admin/index.html");
    }

	@Override
	public void customize(ConfigurableEmbeddedServletContainer container) {
		((TomcatEmbeddedServletContainerFactory) container).addContextCustomizers(new TomcatContextCustomizer()
	    {
	        @Override
	        public void customize(Context context)
	        {
	            context.setUseHttpOnly(true);
	        }
	    });
	}
    
}
