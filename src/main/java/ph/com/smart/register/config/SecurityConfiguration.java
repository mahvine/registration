package ph.com.smart.register.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ph.com.smart.register.security.Http401UnauthorizedEntryPoint;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;
    
    private PasswordEncoder passwordEncoder;
    
    private Http401UnauthorizedEntryPoint entryPoint;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.exceptionHandling().authenticationEntryPoint(entryPoint());
        http.authorizeRequests()
		    	.antMatchers("/").hasAuthority("USER")
		    	.antMatchers("/admin*").hasAuthority("ADMIN")
		    	.antMatchers("/api-docs*").hasAuthority("ADMIN")
		    	.antMatchers("/register/*").authenticated()
		    	.antMatchers("/resources/*").authenticated()
		    	.antMatchers("/registration/*").permitAll()
            	.and().httpBasic().realmName("Restricted area")
            	.and().csrf().disable();
            	
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    	auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
    }
    
    
    @Bean
    public PasswordEncoder passwordEncoder(){
    	if(passwordEncoder==null){
    		passwordEncoder = new BCryptPasswordEncoder();
    	}
    	return passwordEncoder;
    }
    
    @Bean
    public Http401UnauthorizedEntryPoint entryPoint(){
    	if(entryPoint == null){
    		entryPoint = new Http401UnauthorizedEntryPoint();
    	}
    	return entryPoint;
    }
    

    @Bean
    public UserDetailsService userDetailsService(){
     JdbcDaoImpl jdbcImpl = new JdbcDaoImpl();
     jdbcImpl.setDataSource(dataSource);
     jdbcImpl.setUsersByUsernameQuery("select username,password, enabled from users where username=?");
     jdbcImpl.setAuthoritiesByUsernameQuery("select b.username, a.role from user_roles a, users b where b.username=? and a.user_id=b.id");
     return jdbcImpl;
    }
    

}
