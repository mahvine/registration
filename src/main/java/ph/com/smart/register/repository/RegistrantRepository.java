package ph.com.smart.register.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import ph.com.smart.register.model.Registrant;

public interface RegistrantRepository extends JpaRepository<Registrant,Long>,JpaSpecificationExecutor<Registrant>{

	
	
	
}
