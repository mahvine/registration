package ph.com.smart.register.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import ph.com.smart.register.model.Event;

public interface EventRepository extends JpaRepository<Event,Long>, JpaSpecificationExecutor<Event>{
	
	@Modifying
	@Query("update Event e set e.active=false where e.active=true")
	@Transactional
	public void setAllToInActive();

}
