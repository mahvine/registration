package ph.com.smart.register.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import ph.com.smart.register.model.Registrant;
import ph.com.smart.register.model.Requirement;

public interface RequirementRepository extends JpaRepository<Requirement,Long>,JpaSpecificationExecutor<Requirement>{

	
	
	
}
