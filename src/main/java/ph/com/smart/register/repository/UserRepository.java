package ph.com.smart.register.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ph.com.smart.register.model.User;

public interface UserRepository extends JpaRepository<User,Integer>{
	
	public User findByUsername(String username);
}
