package ph.com.smart.register.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ph.com.smart.register.model.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole,Integer>{
	
}
