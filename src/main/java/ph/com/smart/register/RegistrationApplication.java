package ph.com.smart.register;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class RegistrationApplication {
	private static final Logger logger = LoggerFactory.getLogger(RegistrationApplication.class);
    public static void main(String[] args) {

//        SpringApplication app = new SpringApplication(RegistrationApplication.class);
        ConfigurableApplicationContext context = SpringApplication.run(RegistrationApplication.class, args);
        
    }
    
}
