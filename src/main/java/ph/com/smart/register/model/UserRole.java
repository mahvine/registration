/**
 *
 */
package ph.com.smart.register.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Jrdomingo
 * Jan 25, 2016 4:51:28 PM
 * TODO
 */
@Entity
@Table(name="user_roles")
public class UserRole {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer id;
	
	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="id")
	public User user;
	
	public 	String role;

	@Override
	public String toString() {
		return "{" + (id != null ? "id:" + id + ", " : "") + (user != null ? "user:" + user + ", " : "")
				+ (role != null ? "role:" + role : "") + "}";
	}
	
}
