/**
 *
 */
package ph.com.smart.register.model;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @author Jrdomingo
 * Jan 15, 2016 4:28:45 PM
 * TODO
 */
@Entity
@Table(name="event_requirement")
public class Requirement{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public String name;
	
	@ElementCollection
	public List<String> options;
	
	public Type type;
	public enum Type{
		STRING,OPTIONS
	}
}