package ph.com.smart.register.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="event")
public class Event {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;

	public String name;

	public String description;
	
	@Column(name="img_url")
	public String imgUrl;
	
	@Column(name="active")
	public boolean active;
	
	@Column(name="date_created")
	public Date dateCreated;
	
	@Column(name="date_start")
	public Date dateStart;
	
	@Column(name="date_end")
	public Date dateEnd;
	
	@OneToMany
	public List<Requirement> requirements;
	
	@Column(name="success_message")
	public String successMessage;
	
	@Column(name="already_registered_message")
	public String alreadyRegisteredMessage;
	
	@Column(name="footer")
	public String footer;
	
	@Column(name="banner_url")
	public String bannerUrl;
	
	@Transient
	@Enumerated(EnumType.STRING)
	public Status status;
	
	public enum Status{
		ONGOING,UPCOMING,ENDED
	}
	
}
