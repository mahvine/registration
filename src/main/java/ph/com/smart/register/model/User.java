/**
 *
 */
package ph.com.smart.register.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @author Jrdomingo
 * Jan 25, 2016 4:50:08 PM
 */
@Entity
@Table(name="users")
public class User {
	
	@Override
	public String toString() {
		return "{" + (id != null ? "id:" + id + ", " : "") + (username != null ? "username:" + username + ", " : "")
				+ (password != null ? "password:" + password + ", " : "") + "enabled:" + enabled + "}";
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer id;
	
	@Column(unique=true,nullable=false)
	public String username;

	@Column(nullable=false)
	public String password;
	
	@Column(nullable=false)
	public boolean enabled;
	
	public enum Type{
		USER,ADMIN
	}

}
