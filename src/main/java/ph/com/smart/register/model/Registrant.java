package ph.com.smart.register.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name="registrant")
@JsonInclude(Include.NON_NULL)
public class Registrant {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="event_id",referencedColumnName="id")
	public Event event;
	
	@Column(name="full_name")
	public String fullname;

	@Column(name="firstname")
	public String firstname;

	@Column(name="middlename")
	public String middlename;

	@Column(name="lastname")
	public String lastname;
	
	public String email;
	
	public String mobile;
	
	@Column(name="id_number")
	public String idNumber;

	public Date created;

	public Date registered;

	public boolean walkIn;
	
	@Transient
	public boolean register;
	
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "registrant_profile", joinColumns = @JoinColumn(name = "registrant_id"))
	@MapKeyColumn(name = "profile_key", length = 50)
	@Column(name = "profile_val", length = 100)
	@BatchSize(size = 20)
	public Map<Long, String> profile = new HashMap<Long, String>();
	
	@PrePersist
	public void constructFullname(){
		fullname ="";
		if(firstname!=null){
			fullname += StringUtils.capitalize(firstname.trim());
		}

		if(middlename!=null){
			fullname = fullname+" "+StringUtils.capitalize(middlename.trim());
		}
		

		if(lastname!=null){
			fullname = fullname+" "+StringUtils.capitalize(lastname.trim());
		}
		
		fullname = fullname.trim();
	}
	
}
