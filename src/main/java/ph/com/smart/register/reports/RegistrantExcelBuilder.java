package ph.com.smart.register.reports;

import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import ph.com.smart.register.model.Event;
import ph.com.smart.register.model.Registrant;
import ph.com.smart.register.model.Requirement;

public class RegistrantExcelBuilder extends AbstractExcelView {

	private List<Registrant> registrants;
	private Event event;
	private DateFormat df = new SimpleDateFormat("MMM d yyyy hh:mm a");
	
	public RegistrantExcelBuilder(List<Registrant> registrants, Event event) {
		this.registrants = registrants;
		this.event = event;
	}

	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			HSSFWorkbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		// create a new Excel sheet
		HSSFSheet sheet = workbook.createSheet(event.name+" registrants");
		sheet.setDefaultColumnWidth(30);

		// create style for header cells
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName("Arial");
		// style.setFillForegroundColor(HSSFColor.TEAL.index);
		style.setFillForegroundColor(HSSFColor.BLACK.index);
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setColor(HSSFColor.WHITE.index);

		style.setFont(font);

		int rowCount = 0;
		// create header row
		HSSFRow header = sheet.createRow(rowCount++);

		header.createCell(0).setCellValue("Firstname");
		header.getCell(0).setCellStyle(style);

		header.createCell(1).setCellValue("Middlename");
		header.getCell(1).setCellStyle(style);

		header.createCell(2).setCellValue("Lastname");
		header.getCell(2).setCellStyle(style);

		header.createCell(3).setCellValue("Email");
		header.getCell(3).setCellStyle(style);

		header.createCell(4).setCellValue("Walk in?");
		header.getCell(4).setCellStyle(style);

		header.createCell(5).setCellValue("Date registered");
		header.getCell(5).setCellStyle(style);

		
		for(int i =0; i < event.requirements.size();i++){
			Requirement requirement = event.requirements.get(i);
			header.createCell(i+6).setCellValue(requirement.name);
			header.getCell(i+6).setCellStyle(style);
		}
		
		
		// create data rows

		for (Registrant registrant : registrants) {
			HSSFRow aRow = sheet.createRow(rowCount++);
			if(registrant.firstname != null){
				aRow.createCell(0).setCellValue(registrant.firstname);
			}
			
			if(registrant.middlename != null){
				aRow.createCell(1).setCellValue(registrant.middlename );
			}

			if(registrant.lastname != null){
				aRow.createCell(2).setCellValue(registrant.lastname);
			}

			if(registrant.email != null){
				aRow.createCell(3).setCellValue(registrant.email);
			}

			
			String walkIn = "No";
			if(registrant.walkIn){
				walkIn = "Yes";
			}
			aRow.createCell(4).setCellValue(walkIn);
			

			if(registrant.registered != null){
				aRow.createCell(5).setCellValue(df.format(registrant.registered));
			}

			for(int i =0; i < event.requirements.size();i++){
				Requirement requirement = event.requirements.get(i);
				String profile = registrant.profile.get(requirement.id);
				if(profile!=null){
					aRow.createCell(i+6).setCellValue(profile);
				}
			}
			
			

		}

		rowCount += 4;

		// Set the headers
		response.setHeader("Content-Type", "application/octet-stream");
		response.setHeader("Content-Disposition",
				"attachment; filename="+event.name+" registrants.xls");

		// Here is where you will want to put the code to build the Excel
		// spreadsheet
		OutputStream outStream = null;

		try {
			outStream = response.getOutputStream();
			workbook.write(outStream);
			outStream.flush();
		} finally {
			outStream.close();
		}
	}

}
