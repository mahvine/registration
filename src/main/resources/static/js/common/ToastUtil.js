angular.module('ToastUtil',[]).factory('ToastService', [ '$rootScope', function($rootScope) {
    var websocketSocket = atmosphere;
    var websocketSubSocket;
    var websocketTransport = 'websocket';

    this.getUrl = function() {
      return '/websocket/toast';
    }
    
    var websocketRequest = {
      url : this.getUrl(),
      contentType : "application/json",
      transport : websocketTransport,
      trackMessageLength : true,
      withCredentials : true,
      reconnectInterval : 5000,
      enableXDR : true,
      timeout : 60000
    };

    websocketRequest.onOpen = function(response) {
      console.log('Trying to use transport: ' + response.transport);
      websocketTransport = response.transport;
    };

    websocketRequest.onClientTimeout = function(r) {
      setTimeout(function() {
        websocketSubSocket = websocketSocket.subscribe(websocketRequest);
      }, websocketRequest.reconnectInterval);
    };

    websocketRequest.onClose = function(response) {
      console.log('Server closed websocket connection. Changing transport to: '+ response.transport);
    };

    websocketRequest.onMessage = function(data) {
      $rootScope.$apply(function() {
//    	  console.log("onMessage:");
//    	  console.log(data);
    	  $rootScope.receive(data.responseBody);
//	      try {
//	    	 var something = JSON.parse();  
//	    	 if(something){
//		     }
//	      } catch (e) {}
      });
    };

    websocketSubSocket = websocketSocket.subscribe(websocketRequest);
    return {
        doTheThing: "Testing"
    }
  }]);