angular.module('DateUtil',[]).filter('toDatepicker', function($filter) {
	return function(input) {
		if (input == null) {
			return "";
		}
		var _date = $filter('date')(new Date(input), 'MM/dd/yyyy HH:mm a');
		return _date;
	};
}).filter('fromDatepicker', function($filter) {
	return function(input) {
		if (input == null) {
			return "";
		}
		var _date = $filter('date')(new Date(input), 'MM/dd/yyyy HH:mm a');
		var _now = new Date(_date);
		return _now.getTime();
	};
});
