
(function(){
	var app = angular.module('registration',[]);
	
	app.filter('numberFixedLen', function () {
        return function (n, len) {
            var num = parseInt(n, 10);
            len = parseInt(len, 10);
            if (isNaN(num) || isNaN(len)) {
                return n;
            }
            num = ''+num;
            while (num.length < len) {
                num = '0'+num;
            }
            return num;
        };
    });
	
	app.controller('UserController',['$http','$scope',function($http,$scope){
		$scope.style = false;
		$scope.init = function(){
			$scope.mainUser = {}
			$scope.notFound = false;
			$scope.message = "Please enter your:";
			$scope.querying = false;
			$scope.users = [];
			$scope.eventId = 0;
			$http.get("/register/api/events?active=true").success(function(response){
				if(response.length>0){
					$scope.mainEvent = response[0];
					$scope.eventId = $scope.mainEvent.id;
					if($scope.mainEvent.imgUrl){
						$scope.style = {'background-image': 'url('+$scope.mainEvent.imgUrl+');'	};
					}
				}else{
					$scope.mainEvent = {bannerUrl:"/app/img/sample.png",imgUrl:"/app/img/sample.png"};
					$scope.style = {'background-image': 'url('+$scope.mainEvent.imgUrl+');'	};
					
				}
			});
		}
		
		$scope.mainEvent = {bannerUrl:"/app/img/sample.jpg",imgUrl:"/app/img/sample.png"};
		
		$scope.init();
		
		$scope.getUser = function(){
			$scope.notFound=false;
			if($scope.eventId)
			setTimeout(function(){
				var query = '/register/api/events/'+$scope.eventId+'/registrants?test=1';
				if($scope.mainUser.firstname){
					query += "&firstname="+$scope.mainUser.firstname;
				}
				if($scope.mainUser.lastname ){
					query += "&lastname="+$scope.mainUser.lastname;
				}
				if($scope.mainUser.email){
					query += "&email="+$scope.mainUser.email;
				}
				$scope.users = [];
				if($scope.mainUser.lastname!= null && $scope.mainUser.lastname!= ''){
					$scope.querying = true;
					$http.get(query).success(function(response,status){
						$scope.querying = false;
						$scope.users = response;
							
						if($scope.users.length == 1){
							$scope.mainUser = $scope.users[0];
						}else if($scope.users.length ==0 ){
							if( $scope.mainUser.lastname ){
								$scope.notFound = true;
							}
						}else{
							
						}
					});
				}else{
					$scope.querying = false;
				}
			},500);
		};
		$scope.setUser = function(user){
			$scope.mainUser = user;
			$scope.users = [];
			$("#usersModal").modal("hide");
		}
		
		$scope.newUser = function(){
			$scope.notFound = true;
			$scope.users = [];
			$("#usersModal").modal("hide");
		}
		
		$scope.saveUser = function(){
			if(!$scope.mainUser.registered){
				$scope.mainUser.registered = new Date().getTime();
				if(!$scope.mainUser.id){
					$scope.mainUser.walkIn = true;
				}
				$http.post("/register/api/events/"+$scope.eventId+"/registrants",$scope.mainUser)
				.success(function(response){
					alert($scope.mainEvent.successMessage);
					$scope.init();
				});
			}else{
				alert($scope.mainEvent.alreadyRegisteredMessage);
				$scope.init();
			}

		}
		
		$scope.cancel = function(){
			$scope.init();
		}
		
	}]);
	
	
})();