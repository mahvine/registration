'use strict';

/**
 * Route configuration for the RDash module.
 */
angular.module('App').config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        // For unmatched routes
        $urlRouterProvider.otherwise('/events');

        // Application routes
        $stateProvider
            .state('eventslist', {
                url: '/events',
                templateUrl: '/admin/templates/eventlist.html',
                controller: 'EventCtrl'
            })
			.state('registrantslist', {
                url: '/events/{alias}/users',
                templateUrl: '/admin/templates/userlist.html',
                controller: 'EventCtrl'
            });
    }
]);