/**
 * Event Controller
 */

angular
    .module('App')
    .controller('EventCtrl', ['$scope','$http','$state','PagingUtil','$stateParams','Upload', function ($scope, $http,$state, PagingUtil, $stateParams, Upload) {
    	
    	/*********EVENT START********/
    	$scope.events = [];
    	$scope.mainEvent = {name:'',alias:'',description:'',requirements:[],successMessage:'Thank you for registering',alreadyRegisteredMessage:'You have already registered'};
    	$scope.page = 1;
    	$scope.total =0;
    	$scope.listEvents = function(){
    		$http.get("/register/api/events?page="+$scope.page).success(function(response,status,headers){
    			$scope.events = response;
    			$scope.links = PagingUtil.parse(headers('link'));
    			$scope.total = headers("X-Total-Count");
    		});
    	}
    	
    	$scope.setEvent = function(event){
    		if(event){
    			$scope.mainEvent = event;
    			if($scope.mainEvent.dateStart){
    				$scope.mainEvent.dateStart = $scope.formatDate($scope.mainEvent.dateStart);
    			}
    			if($scope.mainEvent.dateStart){
    				$scope.mainEvent.dateEnd = $scope.formatDate($scope.mainEvent.dateEnd);
    			}
    		}else{
    			$scope.mainEvent = {name:'',alias:'',description:'',requirements:[]};
    		}
    	}

		$scope.formatDate = function(millis) {
			var date = new Date(millis);
			return date;
		};
		
    	$scope.saveEvent = function(){
    		$http.post("/register/api/events",$scope.mainEvent).success(function(response){
    			location.reload();
    		});
    	}
    	
    	$scope.addRequirement = function(){
    		$scope.mainEvent.requirements.push({type:"STRING",options:['Yes','No']});
    	}

    	$scope.removeRequirement = function(index){
            $scope.mainEvent.requirements.splice(index, 1);
    	}

    	$scope.addOption = function(requirement){
    		requirement.options.push("");
    	}
    	
    	$scope.removeOption = function(requirement,index){
            requirement.options.splice(index, 1);
    	}
    	

		$scope.upload = function (file,isBanner) {
			$scope.bannerPercentage = false;
			$scope.percentage = false;
	        Upload.upload({
	            url: '/resources',
	            file:file
	        }).progress(function (evt) {
	            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	            if(evt.config){
	            	if(evt.config.file){
	            		console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
	            		if(isBanner){
	            			$scope.bannerPercentage = progressPercentage;
	            		}else{
	            			$scope.percentage = progressPercentage;
	            		}
	            	}
	            }
	        }).success(function (data, status, headers, config) {
	            console.log('file ' + config.file.name + 'uploaded. Response: ');
	            console.log(data);
//	            $scope.mainRewardItem.resourcePath="/resources/"+data.data.resourcePath;
	            if(isBanner){
	            	$scope.mainEvent.bannerUrl="/resources/"+data.data.resourcePath;	            	
	            }else{
	            	$scope.mainEvent.imgUrl="/resources/"+data.data.resourcePath;
	            }
	            /*pollItem.imageFile=data.data.resourcePath;*/
	            $scope.bannerPercentage = false;
				$scope.percentage = false;
	        }).error(function (data, status, headers, config) {
	            console.log('error status: ' + status);
	            $scope.bannerPercentage = false;
				$scope.percentage = false;
	        });
	    };
		
    	
    	/*********EVENT END********/

    	
    	/*********REGISTRANT START*******/
    	$scope.registrants = [];
    	$scope.mainRegistrant = {profile:{}};
    	
    	$scope.setRegistrant = function(registrant){
    		if(registrant){
    			$scope.mainRegistrant = registrant;
    		}else{
    			$scope.mainRegistrant = {profile:{}};
    		}
    	}
    	
    	$scope.listRegistrants = function(){
    		$http.get("/register/api/events/"+$scope.eventAlias+"/registrants?page="+$scope.page)
    			.success(function(response,status, headers){
    				$scope.registrants = response;
        			$scope.links = PagingUtil.parse(headers('link'));
        			$scope.total = headers("X-Total-Count");
    			})
    			.error(function(response){
    				console.log(response);
    			});
    	} 
    	
    	$scope.saveRegistrant = function(){
    		$http.post("/register/api/events/"+$scope.eventAlias+"/registrants",$scope.mainRegistrant)
    			.success(function(response){
    				location.reload();
    			});
    	}
    	
    	
    	/*********REGISTRANT END*******/
    	console.log($state.current.name);
    	console.log($stateParams);
    	$scope.loadPage = function(page){
    		if(page){
    			$scope.page = page;
    		}

    		if($stateParams.alias){
    			$scope.eventAlias = $stateParams.alias;
    		}

    		if($state.current.name=='eventslist'){
    			$scope.listEvents();
    		}

    		if($state.current.name=='registrantslist'){
    			$scope.listRegistrants();
    			$http.get("/register/api/events/"+$scope.eventAlias).success(function(response){
    				$scope.mainEvent = response;
    			});
    		}
    	}
    	$scope.loadPage();
    	
    	
    	
    }]);

